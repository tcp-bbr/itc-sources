#!/bin/bash

# Execute a ns-3 measurement for every config in a given directory

if [[ $# < 1 ]]; then
    echo "usage: execute_all.sh CONFIG_DIR"
    exit 1
fi

configdir=$1

for config in "${configdir}"/*.yaml; do
    [ -e "${config}" ] || continue
    echo "Processing config ${config}"
    ./waf --run "tcp_measurement ${config}"
done