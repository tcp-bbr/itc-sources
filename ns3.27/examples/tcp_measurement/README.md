To run this scipt:

1. Download and build [ns-3](https://www.nsnam.org)
2. Install [yaml-cpp](https://github.com/jbeder/yaml-cpp)
3. Copy this directory to `${NS-3_DIR}/examples`
4. In `${NS-3_DIR}` run:
```
./waf configure --enable-examples
./waf --run tcp_measurement
```