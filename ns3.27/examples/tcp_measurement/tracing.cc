#include "ns3/core-module.h"
#include "ns3/network-module.h"

#include "tracing.h"

using namespace ns3;

void
TraceQueue(Ptr<OutputStreamWrapper> stream, uint32_t oldVal, uint32_t newVal)
{
  *stream->GetStream() << Simulator::Now().GetSeconds() << "; " << newVal << "b" << std::endl;
}

void
WriteCCValues(Ptr<OutputStreamWrapper> stream, int nodeIndex)
{
  *stream->GetStream() << Simulator::Now().GetSeconds() << "; " << currentCwnds[nodeIndex] << "; " << currentSsthreshs[nodeIndex] << ";" << std::endl;
}

void
WriteCCValuesBbr(Ptr<OutputStreamWrapper> stream, int nodeIndex)
{
  *stream->GetStream() << Simulator::Now().GetSeconds() << "; " << currentCwnds[nodeIndex] << "; " << currentSsthreshs[nodeIndex] << "; "
  << "bw:" << currentBtlBws[nodeIndex] << "Mbps,mrtt:" << currentRTprops[nodeIndex] << ","
  << "pacing_gain:" << currentPacingGains[nodeIndex] << ",cwnd_gain:" << currentWindowGains[nodeIndex] << std::endl;
}

void
TraceCwnd(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal)
{
  if (newVal / mss == currentCwnds[nodeIndex]) {
    return;
  }

  currentCwnds[nodeIndex] = newVal / mss;
  WriteCCValues(stream, nodeIndex);
}

void
TraceSstresh(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal)
{
  if (newVal / mss == currentSsthreshs[nodeIndex]) {
    return;
  }
  
  currentSsthreshs[nodeIndex] = newVal / mss;
  WriteCCValues(stream, nodeIndex);
}

void
TraceCwndBbr(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal)
{
  if (newVal / mss == currentCwnds[nodeIndex]) {
    return;
  }

  currentCwnds[nodeIndex] = newVal / mss;
  WriteCCValuesBbr(stream, nodeIndex);
}

void
TraceSstreshBbr(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal)
{
  if (newVal / mss == currentSsthreshs[nodeIndex]) {
    return;
  }
  
  currentSsthreshs[nodeIndex] = newVal / mss;
  WriteCCValuesBbr(stream, nodeIndex);
}

void
TraceBtlBw(Ptr<OutputStreamWrapper> stream, int nodeIndex, double oldVal, double newVal)
{
  currentBtlBws[nodeIndex] = newVal;
  WriteCCValuesBbr(stream, nodeIndex);
}

void
TraceRTprop(Ptr<OutputStreamWrapper> stream, int nodeIndex, double oldVal, double newVal)
{
  currentRTprops[nodeIndex] = newVal;
  WriteCCValuesBbr(stream, nodeIndex);
}

void
TracePacingGain(Ptr<OutputStreamWrapper> stream, int nodeIndex, double oldVal, double newVal)
{
  currentPacingGains[nodeIndex] = newVal;
  WriteCCValuesBbr(stream, nodeIndex);
}

void
TraceWindowGain(Ptr<OutputStreamWrapper> stream, int nodeIndex, double oldVal, double newVal)
{
  currentWindowGains[nodeIndex] = newVal;
  WriteCCValuesBbr(stream, nodeIndex);
}