import os
import numpy as np

from helper.pcap_data import PcapData

from helper import CSV_PATH, CSV_FILE_NAMES
from helper.util import open_compressed_file, find_file

def read_from_csv(path):
    path = os.path.join(path, CSV_PATH)

    data_files = {
        'throughput_file': os.path.join(path, CSV_FILE_NAMES['throughput']),
        'avg_rtt_file': os.path.join(path, CSV_FILE_NAMES['avg_rtt']),
        'fairness_file': os.path.join(path, CSV_FILE_NAMES['fairness']),
        'rtt_file': os.path.join(path, CSV_FILE_NAMES['rtt']),
        'inflight_file': os.path.join(path, CSV_FILE_NAMES['inflight']),
        'sending_rate_file': os.path.join(path, CSV_FILE_NAMES['sending_rate']),
        'bbr_values_file': os.path.join(path, CSV_FILE_NAMES['bbr_values']),
        'bbr_total_values_file': os.path.join(path, CSV_FILE_NAMES['bbr_total_values']),
        'cwnd_values_file': os.path.join(path, CSV_FILE_NAMES['cwnd_values']),
        'retransmissions_file': os.path.join(path, CSV_FILE_NAMES['retransmissions']),
        'retransmissions_interval_file': os.path.join(path, CSV_FILE_NAMES['retransmissions_interval']),
        'buffer_backlog_file': os.path.join(path, CSV_FILE_NAMES['buffer_backlog'])
    }

    throughput = read_csv(data_files['throughput_file'], 2)
    avg_rtt = read_csv(data_files['avg_rtt_file'])
    fairness = read_csv(data_files['fairness_file'], 2)
    rtt = read_csv(data_files['rtt_file'])
    inflight = read_csv(data_files['inflight_file'])
    sending_rate = read_csv(data_files['sending_rate_file'])
    bbr_values = read_csv(data_files['bbr_values_file'], 6)
    bbr_total_values = read_csv(data_files['bbr_total_values_file'])
    cwnd_values = read_csv(data_files['cwnd_values_file'], 3)
    retransmissions = read_csv(data_files['retransmissions_file'], 1)
    retransmissions_interval = read_csv(data_files['retransmissions_interval_file'], 3)
    buffer_backlog = read_csv(data_files['buffer_backlog_file'])

    return PcapData(throughput=throughput,
                    rtt=rtt,
                    fairness=fairness,
                    inflight=inflight,
                    avg_rtt=avg_rtt,
                    sending_rate=sending_rate,
                    bbr_values=bbr_values,
                    bbr_total_values=bbr_total_values,
                    cwnd_values=cwnd_values,
                    retransmissions=retransmissions,
                    retransmissions_interval=retransmissions_interval,
                    buffer_backlog=buffer_backlog)


def read_csv(path, columns_per_connection=2):
    output = {}
    file_path = find_file(path)

    if file_path is None:
        raise IOError('File not found {}'.format(path))

    f = open_compressed_file(find_file(path))

    first_line = f.readline().decode().split(';')[:-1] # python3 bytes vs str
    for line in f:
        line = line.decode()
        split = line.split(';')
        for i in range(0, len(first_line), columns_per_connection):
            if split[i] == '':
                continue

            try:
                index = int(first_line[i])
            except ValueError:
                index = first_line[i]

            if index not in output:
                output[index] = tuple([[] for _ in range(0, columns_per_connection)])
            for column in range(0, columns_per_connection):
                output[index][column].append(float(split[i + column]))
    f.close()
    return output