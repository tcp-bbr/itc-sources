from scipy import interpolate
import numpy as np
import numpy_indexed as npi
import math
import copy

from helper import pcap_data

def group_data(data):
    """
    Group data = (x_list, y_list) by x. Assign (x, y') in output for each unique x in input with y' = mean({y|(x, y) in input})
    """
    return npi.group_by(data[0]).mean(data[1])

def to_relative_time(data):
    return np.subtract(data[0], data[0][0]), data[1]

def interpolate_data(data, start=None, end=None, step=0.2, method='linear', fill_value=np.median):
    """
    1D interpolation, step defines the interval between the x-values of the output
    """
    # Use median as fill value by default to prevent outliers caused by extrapolation
    if start is None:
        start = data[0][0]
    if end is None:
        end = data[0][-1]

    num_sampling_points = math.floor((end - start) / step) + 1 # include endpoint
    t = np.linspace(start, end, num=num_sampling_points)
    f = interpolate.interp1d(data[0], data[1], fill_value=fill_value(data[1]), bounds_error=False, kind=method)
    return t, f(t)

def shift_timestamps(data):
    """
    Transform absolute timestamps in pcap data to relative timestamps
    """
    t_min = data.get_min_ts()
    data = data.values_as_dict()

    for v in data:
        for c in data[v]:
            data[v][c][0][:] = [x - t_min for x in data[v][c][0]]

    return pcap_data.PcapData.from_dict(data)

def interpolate_pcap_data(data, start=None, end=None, step=0.2, method='linear', fill_value=np.median):
    """
    Interpolate an entire pcap_data object
    """
    min_ts = data.get_min_ts()
    max_ts = data.get_max_ts()
    if start is None:
        start = min_ts
    if end is None:
        end = max_ts

    if start < min_ts or end > max_ts:
        raise ValueError('Start and end must be within the bounds of pcap data')

    data = copy.deepcopy(data.values_as_dict())

    for v in data:
        if v in {'retransmissions', 'retransmissions_interval'}:
            continue
        for c in data[v]:
            updatedT = False
            t = data[v][c][0][:]
            for i, d in enumerate(data[v][c][1:]):
                t_inter, y = interpolate_data(group_data((t, d)), start, end, step, method, fill_value)
                if not updatedT:
                    data[v][c][0][:] = t_inter
                    updatedT = True
                data[v][c][i + 1][:] = y

    return pcap_data.PcapData.from_dict(data)

def normalize_pcap_data(data):
    """
    Normalize a pcap_data object using the normalize() method
    """
    data = copy.deepcopy(data.values_as_dict())

    for v in data:
        if v in {'retransmissions', 'retransmissions_interval'}:
            continue
        for c in data[v]:
            for i, d in enumerate(data[v][c][1:]):
                data[v][c][i + 1][:] = normalize(d)

    return pcap_data.PcapData.from_dict(data)

def normalize_pcap_data2(data1, data2):
    """
    Normalize two pcap_data objects using the normalize2() method
    """
    num_flows1 = len(data1.sending_rate.keys()) - 1
    num_flows2 = len(data2.sending_rate.keys()) - 1
    assert(num_flows1 == num_flows2)

    data1, data2 = copy.deepcopy(data1.values_as_dict()), copy.deepcopy(data2.values_as_dict())

    for v1, v2 in zip(data1, data2):
        assert(v1 == v2)
        if v1 in {'retransmissions', 'retransmissions_interval'}:
            continue
        elif v1 == 'fairness' and num_flows1 <= 1:
            continue
        for c1, c2 in zip(data1[v1], data2[v2]):
            assert(c1 == c2)
            for i, (d1, d2) in enumerate(zip(data1[v1][c1][1:], data2[v2][c2][1:])):
                normal = normalize2(d1, d2)
                data1[v1][c1][i + 1][:] = normal[0]
                data2[v2][c2][i + 1][:] = normal[1]

    return pcap_data.PcapData.from_dict(data1), pcap_data.PcapData.from_dict(data2)

def normalize(data):
    """
    Transform data to [0, 1]
    """
    max_val = max(data)
    min_val = min(data)
    interval = max_val - min_val
    return (np.asarray(data) - min_val) / interval

def normalize2(data1, data2):
    """
    Transform 2 pieces of data to [0, 1]
    """
    max_val = max(max(data1), max(data2))
    min_val = min(min(data1), min(data2))
    interval = max_val - min_val
    return (np.asarray(data1) - min_val) / interval, (np.asarray(data2) - min_val) / interval