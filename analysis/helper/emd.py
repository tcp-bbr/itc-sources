import numpy as np
import matplotlib.pyplot as plt
import pyemd

from helper import csv_writer3 as csv_writer
from helper import pcap_data
from helper import pcap_utils

# ============== taken from pyemd
def euclidean_pairwise_distance_matrix(x):
    """Calculate the Euclidean pairwise distance matrix for a 1D array."""
    distance_matrix = np.abs(np.repeat(x, len(x)) - np.tile(x, len(x)))
    return distance_matrix.reshape(len(x), len(x))
# ===============================

def manhattan_pairwise_distance_matrix(x):
    """Calculate the Manhattan pairwise distance matrix for a 1D array."""
    distance_matrix = np.abs(np.repeat(np.arange(len(x)), len(x)) - np.tile(np.arange(len(x)), len(x)))
    return distance_matrix.reshape(len(x), len(x))

def emd_time_series(data1, data2):
    """
    Calculate the earth mover's distance for two time series
    """
    assert(len(data1[0]) == len(data2[0]))
    assert(np.equal(data1[0], data2[0]).all())
    assert(len(data1[1]) == len(data2[1]))
    assert(len(data1[0]) == len(data1[1]))
    assert(0 <= min(data1[1]) and 1 >= max(data1[1]))
    assert(0 <= min(data2[1]) and 1 >= max(data2[1]))
    assert(max(data1[1]) == 1 or max(data2[1]) == 1)
    assert(min(data1[1]) == 0 or min(data2[1]) == 0)

    distance_matrix = euclidean_pairwise_distance_matrix(data1[0])
    sum1 = np.sum(data1[1])
    sum1 = sum1 if sum1 != 0 else 1 # prevent divide by 0
    sum2 = np.sum(data2[1])
    sum2 = sum2 if sum2 != 0 else 1
    max_distance = np.max(distance_matrix)
    max_distance = max_distance if max_distance != 0 else 1
    emd = pyemd.emd(np.asarray(data1[1]), np.asarray(data2[1]), distance_matrix)
    return emd / (max_distance * max(sum1, sum2))


def emd_distribution_function(data1, data2, bins=1001):
    """
    Calculate the earth mover's distance for two sample sets, ignoring time
    """
    assert(len(data1[1]) == len(data2[1]))
    assert(0 <= min(data1[1]) and 1 >= max(data1[1]))
    assert(0 <= min(data2[1]) and 1 >= max(data2[1]))
    assert(max(data1[1]) == 1 or max(data2[1]) == 1)
    assert(min(data1[1]) == 0 or min(data2[1]) == 0)

    data1 = data1[1]
    data2 = data2[1]

    emd = pyemd.emd_samples(data1, data2, distance=euclidean_pairwise_distance_matrix, bins=bins, range=(0, 1))
    # resulting emd is already normalized as the maximum distance is 1
    return emd
