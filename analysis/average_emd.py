import numpy as np
import os
import argparse
import copy

from helper import csv_writer3 as csv_writer
from helper import pcap_data
from helper import pcap_utils
from helper import CSV_PATH
from helper import emd

def read_dir(path):
    if os.path.isdir(os.path.join(path, CSV_PATH)):
        # single dir
        return [csv_writer.read_from_csv(path)]
    else:
        # multiple dirs
        return [csv_writer.read_from_csv(os.path.join(path, dir)) for dir in os.listdir(path)]

def flatten_bbr(data):
    bbr = data['bbr_values']
    for i, name in enumerate(['btlbw', 'rtprop', 'pacing_gain', 'cwnd_gain', 'bdp']):
        data[name] = {k: (bbr[k][0], bbr[k][i + 1]) for k, v in bbr.items()}
    del data['bbr_values']

def main():
    description = """
    Compute the EMD between two result sets.
    Multiple experiments can be processed together by specifying the parent directory in DIREM or DIRSIM.
    In this case the EMD is averaged over the experiments.
    """

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('direm', metavar='DIREM', help='Input Directory (Emulation)')
    parser.add_argument('dirsim', metavar='DIRSIM', help='Input Directory (Simulation)')
    parser.add_argument('-s', metavar='STEP', type=float, dest='step', default=0.05, help='Step size')
    parser.add_argument('--start', metavar='START', type=float, dest='start', default=None, help='Start of interpolation')
    parser.add_argument('--end', metavar='END', type=float, dest='end', default=None, help='End of interpolation')

    args = parser.parse_args()

    start = args.start
    end = args.end
    step = args.step
    method = 'linear'

    print(f'Interpolation: start: {start}, end: {end}, step: {step}')

    em_data = read_dir(args.direm)
    sim_data = read_dir(args.dirsim)

    em_data = [pcap_utils.interpolate_pcap_data(pcap_utils.shift_timestamps(data), start, end, step, method) for data in em_data]
    sim_data = [pcap_utils.interpolate_pcap_data(pcap_utils.shift_timestamps(data), start, end, step, method) for data in sim_data]
    
    emd_d = {}
    emd_t = {}

    for i, sim in enumerate(sim_data):
        for j, em in enumerate(em_data):
            print(f'-------{i + j}-------')
            
            em_n, sim_n = (d.values_as_dict() for d in pcap_utils.normalize_pcap_data2(em, sim))
            flatten_bbr(em_n)
            flatten_bbr(sim_n)

            for (em_name, em_data), (sim_name, sim_data) in zip(em_n.items(), sim_n.items()):
                assert(em_name == sim_name)
                if em_name in {"bbr_total_values", "fairness", "retransmissions", "retransmissions_interval"}:
                    continue

                if not 0 in em_data and not 0 in sim_data:
                    continue
                
                em_data = em_data[0]
                sim_data = sim_data[0]

                if i == 0 and j == 0:
                    emd_d[em_name] = []
                    emd_t[em_name] = []

                print(em_name)

                distance = emd.emd_distribution_function(em_data, sim_data)
                print("EMD_D: {} (rounded: {:.3f})".format(distance, distance))
                emd_d[em_name].append(distance)

                distance = emd.emd_time_series(em_data, sim_data)
                print("EMD_T: {} (rounded: {:.3f})".format(distance, distance))
                emd_t[em_name].append(distance)

    print('-------Average-------')
    for (name_d, emd_dl), (name_t, emd_tl) in zip(emd_d.items(), emd_t.items()):
        assert(name_d == name_t)
        print(name_d)
        print(f"EMD_D: {np.average(emd_dl)} stddev: {np.std(emd_dl)}")
        print(f"EMD_T: {np.average(emd_tl)} stddev: {np.std(emd_tl)}")

if __name__ == "__main__":
    main()
