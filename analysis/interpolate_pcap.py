import os
import argparse

from helper import csv_writer
from helper import pcap_data
from helper import pcap_utils

def main():
    parser = argparse.ArgumentParser(description='Interpolate the data to obtain evenly spaced measurment points.')
    parser.add_argument('dir', metavar='DIR', help='Input Directory')
    parser.add_argument('-s', metavar='STEP', type=float, dest='step', default=0.05, help='Step size')

    args = parser.parse_args()

    data = csv_writer.read_from_csv(args.dir)
    data = pcap_utils.shift_timestamps(data)
    data = pcap_utils.interpolate_pcap_data(data, step=args.step, fill_value=lambda x: x[0])
    csv_writer.write_to_csv(args.dir, data, 'none', 'csv_interpolated')


if __name__ == "__main__":
    main()