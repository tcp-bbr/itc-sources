import numpy as np
import os
import argparse
import copy

from helper import csv_writer3 as csv_writer
from helper import pcap_data
from helper import pcap_utils
from helper import CSV_PATH
import emd

def read_dir(path):
    if os.path.isdir(os.path.join(path, CSV_PATH)):
        # single dir
        return {os.path.basename(path): csv_writer.read_from_csv(path)}
    else:
        # multiple dirs
        return {os.path.basename(dir): csv_writer.read_from_csv(os.path.join(path, dir)) for dir in os.listdir(path) if os.path.basename(dir) != 'averages'}

def main():
    metrics = {"sending_rate", "throughput", "fairness"}
    
    parser = argparse.ArgumentParser(description='Compute average sending rate, throughput and fairness')
    parser.add_argument('inputdir', metavar='INPUTDIR', help='Input Directory')
    parser.add_argument('--start', metavar='START', type=float, dest='start', default=0, help='Starttime')
    parser.add_argument('--end', metavar='END', type=float, dest='end', default=float('inf'), help='Endtime')

    args = parser.parse_args()

    data = read_dir(args.inputdir)

    outputdir = os.path.join(args.inputdir, 'averages')
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)

    files = {m: open(os.path.join(outputdir, m + '.csv'), 'w') for m in metrics}

    try:
        for (name, d) in data.items():
            for f in files.values():
                f.write(f"{name};")       
            
            datadict = d.values_as_dict()

            for (metric, values) in datadict.items():
                if metric not in metrics:
                    continue
                
                f = files[metric]

                for c in values:
                    # values[c][0] is time
                    inrange = [y for (t, y) in zip(values[c][0], values[c][1]) if t >= args.start and t <= args.end]
                    if len(inrange) > 0:
                        average = sum(inrange) / len(inrange)
                    else:
                        average = 0
                    f.write(f"{c};{average};")
            for f in files.values():
                f.write('\n')
    finally:
        for f in files.values():
            f.close()

if __name__ == "__main__":
    main()
