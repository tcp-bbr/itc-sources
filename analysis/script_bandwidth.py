from helper.csv_writer import read_csv
import sys
import os
import numpy as np
import argparse

# adapted from https://gitlab.lrz.de/tcp-bbr/journal-raw-data/-/blob/08c5c4ac8a19a9fea510e2c48ebe60728a88d18c/Figure-09/script_buffersize.py

def parse_values(dir, name):
    output = {}
    
    with open(os.path.basename(dir) + '_' + name + '.csv', 'w') as f:
        for path, dirs, files in os.walk(dir):
            if 'csv_data' not in dirs:
                continue
            try:
                split = os.path.basename(path).split('_')
                x_value = split[-1].replace('BDP', '').replace('ms', '') # x_value is in multiples of BDP (for varying buffersize) or in ms (for RTT unfairness)


            except Exception as e:
                sys.stderr.write('Skipping directory {}\n{}\n'.format(path, e))
                continue

            key = x_value

            if not key in output.keys():
                output[key] = ([], [])

            if not os.path.exists(os.path.join(path, 'csv_data/throughput.csv.gz')):
                print('Skipping {}'.format(path))

            data = read_csv(os.path.join(path, 'csv_data/throughput.csv.gz'))
            fairness = read_csv(os.path.join(path, 'csv_data/fairness.csv.gz'))['Throughtput'][1]

            avg_fairness = np.median(fairness[int(len(fairness)*0.8):])

            if avg_fairness > 0.999 or np.abs(avg_fairness - 0.5) < 0.01:
                # something might have went wrong with this data
                print(key, path)

            # for bbr vs cubic: first flow is always cubic
            # for RTT unfairness: first flow is always the one with constant RTT
            output[key][0].append(np.mean(data[0][1]) / np.mean(data[2][1])) # first flow
            output[key][1].append(np.mean(data[1][1]) / np.mean(data[2][1])) # second flow

        f.write('key;first;second;fairness;std_first;std_second\n')
        for key in sorted(output.keys()):
            first_values = output[key][0]
            second_values = output[key][1]

            fairness = (np.mean(first_values) + np.mean(second_values)) ** 2 / 2 / \
                    (np.mean(first_values) ** 2 + np.mean(second_values) ** 2)

            values = [
                key,
                np.mean(first_values),
                np.mean(second_values),
                fairness,
                np.std(first_values),
                np.std(second_values)
            ]

            f.write(';'.join(map(str, values)) + '\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Calculate average throughput share for competing flows. Used for Figures 7 and 10.')
    parser.add_argument('directory', metavar='DIR', help='Input directory of the results.')
    parser.add_argument('-n', metavar='NAME', default='throughput_share', help='Name of the output file')

    args = parser.parse_args()
    
    parse_values(args.directory, args.n)
