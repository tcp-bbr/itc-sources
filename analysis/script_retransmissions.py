from helper.csv_writer import read_csv
import sys
import os
import numpy as np
import argparse

# adapted from https://gitlab.lrz.de/tcp-bbr/journal-raw-data/-/blob/08c5c4ac8a19a9fea510e2c48ebe60728a88d18c/Figure-07/script_retransmissions.py

def get_retransmission_rate(dir, name, t_sync):
    output = {}

    with open(os.path.basename(dir) + '_' + name + '.csv', 'w') as f:
        for path, dirs, files in os.walk(dir):
            if 'csv_data' not in dirs:
                continue
            try:
                split = os.path.basename(path).split('_')
                buffer = split[-1].replace('BDP', '')

            except Exception as e:
                sys.stderr.write('Skipping directory {}\n{}\n'.format(path, e))
                continue

            key = buffer

            if not key in output.keys():
                output[key] = ([], [])

            if not os.path.exists(os.path.join(path, 'csv_data/retransmissions_interval.csv.gz')):
                sys.stderr.write('Skipping {}\n'.format(path))

            retransmissions = read_csv(os.path.join(path, 'csv_data/retransmissions_interval.csv.gz'), 3)

            packets_start = 0
            retrans_start = 0
            packets_end = 0
            retrans_end = 0
            for c in retransmissions:

                start = 0

                for i,t in enumerate(retransmissions[c][0]):
                    if t > t_sync:
                        break
                    start = i

                packets_start += sum(retransmissions[c][2][:start])
                packets_end += sum(retransmissions[c][2][start:])

                retrans_start += sum(retransmissions[c][1][:start])
                retrans_end += sum(retransmissions[c][1][start:])

            output[key][0].append(float(retrans_start) / packets_start)
            output[key][1].append(float(retrans_end) / packets_end)

        f.write('buffersize;rate_t<tsync;rate_t>=tsync\n')
        for key in sorted(output.keys(), key=lambda x: float(x)):
            f.write(';'.join(map(str, [key, np.mean(output[key][0]), np.mean(output[key][1])])) + '\n')

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Calculate average retransmission rate before and after TSYNC. Used for Figure 8.')
    parser.add_argument('directory', metavar='DIR', help='Directory of the results')
    parser.add_argument('-n', metavar='NAME', default='retransmission_rate', help='Name of the output file')
    parser.add_argument('--sync', metavar='TSYNC', default=25, help='Synchronization point (in seconds)')

    args = parser.parse_args()
    
    get_retransmission_rate(args.directory, args.n, args.sync)
