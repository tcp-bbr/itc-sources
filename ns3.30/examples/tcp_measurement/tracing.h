#ifndef TRACING_H
#define TRACING_H

#include "ns3/core-module.h"

using namespace ns3;

// The tracing functions only get one of the values stored to the .bbr file but still need to write the others --> keep track of the current values here
extern uint32_t* currentCwnds;
extern uint32_t* currentSsthreshs;

void TraceQueue(Ptr<OutputStreamWrapper> stream, uint32_t oldVal, uint32_t newVal);

void WriteCCValues(Ptr<OutputStreamWrapper> stream, int nodeIndex);

void TraceCwnd(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal);

void TraceSstresh(Ptr<OutputStreamWrapper> stream, int nodeIndex, uint32_t mss, uint32_t oldVal, uint32_t newVal);

#endif // TRACING_H