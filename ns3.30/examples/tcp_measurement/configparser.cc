#include <algorithm>
#include <string>
#include <vector>

#include "ns3/core-module.h"
#include "ns3/network-module.h"

#include "yaml-cpp/yaml.h"

#include "configparser.h"

using namespace ns3;

DataRate
ConvertToDataRate(std::string s)
{
  std::string::size_type beginUnit = s.find_first_not_of("0123456789.");
  if (beginUnit != std::string::npos) {
    std::string unit = s.substr(beginUnit, std::string::npos);
    std::transform(unit.begin(), unit.end(), unit.begin(), [](unsigned char c){ return std::tolower(c); }); // to lowercase (https://stackoverflow.com/questions/313970/how-to-convert-stdstring-to-lower-case)

    if(unit == "bit" || unit == "bps") {
      s.replace(beginUnit, std::string::npos, "bps");
    } else if(unit == "kbit" || unit == "kbps") {
      s.replace(beginUnit, std::string::npos, "kbps");
    } else if(unit == "mbit" || unit == "mbps") {
      s.replace(beginUnit, std::string::npos, "Mbps");
    } else if(unit == "gbit" || unit == "gbps") {
      s.replace(beginUnit, std::string::npos, "Gbps");
    } else {
      NS_FATAL_ERROR("Unknown data rate unit: " + unit);
    }

    return DataRate(s);
  }
  NS_FATAL_ERROR("No data rate unit specified");
}

QueueSize
ConvertLatencyToBytes(Time latency, DataRate rate) {
  uint32_t size = (rate.GetBitRate() * latency.GetMilliSeconds()) / (8 * 1000); // Convert latency to QueueSize in bytes using bitrate
  return QueueSize(QueueSizeUnit::BYTES, size); 
}

void
ParseConfig(std::string fileName, struct configuration* config)
{
  YAML::Node conf = YAML::LoadFile(fileName);

  if (conf["bw"]) {
    config->bw = ConvertToDataRate(conf["bw"].as<std::string>());
  } else {
    config->bw = DataRate("10Mbps");
  }

  if (conf["rtt"]) {
    config->rtt = Time(conf["rtt"].as<std::string>());
  } else {
    config->rtt = Time("50ms");
  }

  if (conf["loss"]) {
    config->loss = conf["loss"].as<double>() / 100.0;
  } else {
    config->loss = 0;
  }

  bool latencySet = false;
  Time latency;
  if (conf["buffer_size"]) {
    config->bufferSize = QueueSize(conf["buffer_size"].as<std::string>());
  } else if (conf["latency"]) {
    latencySet = true;
    latency = Time(conf["latency"].as<std::string>());
    config->bufferSize = ConvertLatencyToBytes(latency, config->bw);
  } else {
    config->bufferSize = QueueSize("125KB");
  }

  if (!conf["events"]) {
    return;
  }

  if (!conf["events"].IsSequence()) {
    NS_FATAL_ERROR("Events is not an array.");
  }

  YAML::Node events = conf["events"];
  for (YAML::Node event : events) {
    if (!event["type"]) {
      NS_FATAL_ERROR("No type specified for event.");
    }

    std::string type = event["type"].as<std::string>();
    if (type == "flow") {
      if (event["start"] && event["stop"] && event["algorithm"]) {
        struct configFlow flow;
        flow.start = Seconds(event["start"].as<int>());
        flow.stop = Seconds(event["stop"].as<int>());
        std::string algorithm = event["algorithm"].as<std::string>();
        if (!TypeId::LookupByNameFailSafe(algorithm, &flow.algorithm)) {
          NS_FATAL_ERROR("Unknown congestion control algorithm: " + algorithm);
        }

        if (event["rtt"]) {
          flow.rtt = Time(event["rtt"].as<std::string>());
        } else {
          flow.rtt = Time("0ms");
        }

        config->flows.push_back(flow);
      } else {
        NS_FATAL_ERROR("Malformed flow event.");
      }
    } else if (type == "link_change") {
      if (event["start"] && (event["rtt"] || event["bw"])) {
        struct configLinkChange linkChange;
        linkChange.start = Seconds(event["start"].as<int>());

        if (event["rtt"]) {
          linkChange.hasRtt = true;
          linkChange.rtt = Time(event["rtt"].as<std::string>());
        } else {
          linkChange.hasRtt = false;
        }

        if (event["bw"]) {
          linkChange.hasBw = true;
          linkChange.bw = ConvertToDataRate(event["bw"].as<std::string>());
          // If buffer size is specified as latency recalculate queue size and schedule change accordingly
          if (latencySet) {
            linkChange.hasBufferSize = true;
            linkChange.bufferSize = ConvertLatencyToBytes(latency, linkChange.bw);
          } else {
            linkChange.hasBufferSize = false;
          }
        } else {
          linkChange.hasBw = false;
          linkChange.hasBufferSize = false;
        }

        config->linkChanges.push_back(linkChange);
      } else {
        NS_FATAL_ERROR("Malformed link_change event.");
      }
    } else {
      NS_FATAL_ERROR("Unknown event type: " + type);
    }
  }
}