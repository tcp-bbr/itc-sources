#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <string>
#include <vector>

#include "ns3/core-module.h"
#include "ns3/network-module.h"

using namespace ns3;

struct configFlow {
  Time start;
  Time stop;
  Time rtt;
  TypeId algorithm;
};

struct configLinkChange {
  Time start;
  bool hasRtt;
  Time rtt;
  bool hasBw;
  DataRate bw;
  bool hasBufferSize;
  QueueSize bufferSize;
};

struct configuration {
  DataRate bw;
  Time rtt;
  double loss;
  QueueSize bufferSize;
  std::vector<struct configFlow> flows;
  std::vector<struct configLinkChange> linkChanges;
};

DataRate ConvertToDataRate(std::string s);

QueueSize ConvertLatencyToBytes(Time latency, DataRate rate);

void ParseConfig(std::string fileName, struct configuration* config);

#endif // CONFIGPARSER_H