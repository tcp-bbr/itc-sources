/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <chrono>
#include <ctime>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"

#include "configparser.h"
#include "tracing.h"

#define NUM_ROUTERS 3

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TcpMeasurement");

std::string
GetDateTime(const char* format)
{
  time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  std::ostringstream oss;
  oss << std::put_time(std::localtime(&now), format);
  return oss.str();
}

void
MakeDirectories(std::string path)
{
  mode_t mode = 0755; // rwxr-xr-x
  int ret = mkdir(path.c_str(), mode);
  if (ret == 0 || errno == EEXIST) {
    return;
  } else if (errno == ENOENT) {
    size_t slash = path.find_last_of('/');
    if (slash == std::string::npos) {
      NS_FATAL_ERROR("Could not create directory: " + path);
    }
    MakeDirectories(path.substr(0, slash));
    if (mkdir(path.c_str(), mode) != 0) {
      NS_FATAL_ERROR("Could not create directory: " + path);
    }
  } else {
    NS_FATAL_ERROR("Could not create directory: " + path);
  }
}

// See https://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way
void
CopyFile(std::string src, std::string dst)
{
  // Assume target directory already exists
  std::ifstream ssrc(src, std::ios::binary);
  std::ofstream sdst(dst, std::ios::binary);
  sdst << ssrc.rdbuf();
}

void
HandleLinkChange(struct configLinkChange event)
{
  if (event.hasBw) {
    Config::Set("NodeList/1/DeviceList/1/$ns3::PointToPointNetDevice/DataRate", DataRateValue(event.bw)); // left bottleneck router
    Config::Set("NodeList/2/DeviceList/0/$ns3::PointToPointNetDevice/DataRate", DataRateValue(event.bw)); // right bottleneck router
  }

  if (event.hasRtt) {
    Config::Set("ChannelList/1/$ns3::PointToPointChannel/Delay", TimeValue(event.rtt / 2)); // bottleneck channel
  }
  
  if (event.hasBufferSize) {
    // cannot make queue smaller than current size
    Config::MatchContainer c = Config::LookupMatches("NodeList/1/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue");
    if (c.GetN() < 1) {
      NS_FATAL_ERROR("No bottleneck queue found");
    }
    Ptr<Queue<Packet>> bnQueue = DynamicCast<Queue<Packet>>(c.Get(0));
    if (bnQueue->GetCurrentSize() > event.bufferSize) {
      while (bnQueue->GetCurrentSize() > event.bufferSize) {
        bnQueue->Remove();
      }
      bnQueue->Remove(); // Remove an extra packet, otherwise ns-3 sometimes reports "BUG! No room in the device queue for the received packet!"
    }
    Config::Set("NodeList/1/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue/MaxSize", QueueSizeValue(event.bufferSize)); // left bottleneck router
  }
}

// The tracing functions only get one of the values stored to the .bbr file but still need to write the others --> keep track of the current values here
uint32_t* currentCwnds;
uint32_t* currentSsthreshs;

void
StartTrace(Ptr<OutputStreamWrapper> streamCCValues, int nodeIndex, uint32_t mss)
{
  std::string socketPath = "/NodeList/" + std::to_string(nodeIndex + NUM_ROUTERS) + "/$ns3::TcpL4Protocol/SocketList/0/";
  Config::ConnectWithoutContext(socketPath + "CongestionWindow", MakeBoundCallback(&TraceCwnd, streamCCValues, nodeIndex, mss));
  Config::ConnectWithoutContext(socketPath + "SlowStartThreshold", MakeBoundCallback(&TraceSstresh, streamCCValues, nodeIndex, mss));
}

int
main(int argc, char *argv[])
{ 
  CommandLine cmd;
  cmd.Parse(argc, argv);
  
  Time::SetResolution(Time::NS);
  LogComponentEnable("TcpMeasurement", LOG_LEVEL_INFO);
  LogComponentEnable("Config", LOG_LEVEL_WARN);
  LogComponentEnableAll(LOG_LEVEL_ERROR);

  // 2 MiB of TCP buffer else flow control is limiting
  Config::SetDefault("ns3::TcpSocket::RcvBufSize", UintegerValue(1 << 21));
  Config::SetDefault("ns3::TcpSocket::SndBufSize", UintegerValue(1 << 21));

  std::string configFilePath = "./config.yaml";
  if (argc > 1) {
    configFilePath = argv[1];
  }

  NS_LOG_INFO("Parsing configuration: " << configFilePath);
  
  std::string configName = configFilePath.substr(configFilePath.find_last_of('/') + 1, configFilePath.find_last_of('.') - configFilePath.find_last_of('/') - 1);

  struct configuration conf;
  ParseConfig(configFilePath, &conf);

  NS_LOG_INFO("Creating nodes.");
  
  NodeContainer routers;
  routers.Create(NUM_ROUTERS);

  int numFlows = conf.flows.size();
  NodeContainer senders;
  senders.Create(numFlows);

  NodeContainer receivers;
  receivers.Create(numFlows);

  NS_LOG_INFO("Building dumbbell topology.");

  NetDeviceContainer senderDevices;
  NetDeviceContainer receiverDevices;
  NetDeviceContainer r0Devices;
  NetDeviceContainer r1Devices;
  NetDeviceContainer r2Devices;
  
  uint16_t mtu = 1500;
  Config::SetDefault("ns3::PointToPointNetDevice::Mtu", UintegerValue(mtu));

  PointToPointHelper toBottleneck;
  toBottleneck.SetDeviceAttribute("DataRate", StringValue("10Gbps"));
  toBottleneck.SetChannelAttribute("Delay", StringValue("0ms"));

  NetDeviceContainer toBnDevices = toBottleneck.Install(NodeContainer(routers.Get(0), routers.Get(1)));
  r0Devices.Add(toBnDevices.Get(0));
  r1Devices.Add(toBnDevices.Get(1));

  PointToPointHelper bottleneck;
  bottleneck.SetDeviceAttribute("DataRate", DataRateValue(conf.bw));
  bottleneck.SetChannelAttribute("Delay", TimeValue(conf.rtt / 2));

  // See examples/error-model/simple-error-model.cc
  if (conf.loss > 0 && conf.loss <= 1) {
    ObjectFactory factory;
    factory.SetTypeId("ns3::RateErrorModel");
    Ptr<ErrorModel> errorModel = factory.Create<ErrorModel>();
    errorModel->SetAttribute("ErrorUnit", EnumValue(RateErrorModel::ERROR_UNIT_PACKET));
    errorModel->SetAttribute("ErrorRate", DoubleValue(conf.loss));
    bottleneck.SetDeviceAttribute("ReceiveErrorModel", PointerValue(errorModel));
  }

  NetDeviceContainer bnDevices = bottleneck.Install(NodeContainer(routers.Get(1),routers.Get(2)));
  r1Devices.Add(bnDevices.Get(0));
  r2Devices.Add(bnDevices.Get(1));

  PointToPointHelper zeroDelay;
  zeroDelay.SetDeviceAttribute("DataRate", StringValue("10Gbps"));
  zeroDelay.SetChannelAttribute("Delay", StringValue("0ms"));

  for (int i = 0; i < numFlows; i++) {
    NetDeviceContainer sendLink = zeroDelay.Install(NodeContainer(routers.Get(0), senders.Get(i)));
    r0Devices.Add(sendLink.Get(0));
    senderDevices.Add(sendLink.Get(1));

    PointToPointHelper perFlowDelay;
    perFlowDelay.SetDeviceAttribute("DataRate", StringValue("10Gbps"));
    perFlowDelay.SetChannelAttribute("Delay", TimeValue(conf.flows[i].rtt / 2));
    NetDeviceContainer recvLink = perFlowDelay.Install(NodeContainer(routers.Get(2), receivers.Get(i)));
    r2Devices.Add(recvLink.Get(0));
    receiverDevices.Add(recvLink.Get(1));
  }

  NS_LOG_INFO("Configuring IP addresses.");

  InternetStackHelper stack;
  stack.InstallAll();

  Ipv4AddressHelper transportAddress;
  transportAddress.SetBase("192.168.1.0", "255.255.255.0", "0.0.0.1");
  transportAddress.Assign(toBnDevices);
  transportAddress.NewNetwork();
  transportAddress.Assign(bnDevices);

  Ipv4InterfaceContainer senderInterfaces;
  Ipv4InterfaceContainer receiverInterfaces;
  Ipv4AddressHelper senderAddress;
  senderAddress.SetBase("10.0.0.0", "255.255.255.0", "0.0.0.1");
  Ipv4AddressHelper receiverAddress;
  receiverAddress.SetBase("11.0.0.0", "255.255.255.0", "0.0.0.1");
  for (int i = 0; i < numFlows; i++) {
    NetDeviceContainer sendNet;
    sendNet.Add(senderDevices.Get(i));
    sendNet.Add(r0Devices.Get(i + 1));
    Ipv4InterfaceContainer sendIfaces = senderAddress.Assign(sendNet);
    senderInterfaces.Add(sendIfaces.Get(0));

    NetDeviceContainer recvNet;
    recvNet.Add(receiverDevices.Get(i));
    recvNet.Add(r2Devices.Get(i + 1));
    Ipv4InterfaceContainer recvIfaces = receiverAddress.Assign(recvNet);
    receiverInterfaces.Add(recvIfaces.Get(0));

    senderAddress.NewNetwork();
    receiverAddress.NewNetwork();
  }

  NS_LOG_INFO("Configuring queues.");
  
  TrafficControlHelper tch;
  tch.Uninstall(r1Devices.Get(1)); // Remove qdisc, interferes with TxQueue (causes "plateaus" in buffer backlog)
  Config::Set("NodeList/1/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue/MaxSize", QueueSizeValue(conf.bufferSize));

  NS_LOG_INFO("Configuring TCP.");

  uint32_t mss = mtu - 20 - 40; // 20B IP header, 40B for TCP according to RFC 879
  Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(mss));

  for (int i = 0; i < numFlows; i++) {
    Config::Set("/NodeList/" + std::to_string(i + NUM_ROUTERS) + "/$ns3::TcpL4Protocol/SocketType", TypeIdValue(conf.flows[i].algorithm));
  }

  NS_LOG_INFO("Setting up applications.");
  
  uint16_t port = 9000;

  Time experimentDuration = Seconds(0.0);

  ApplicationContainer senderApps;
  for (int i = 0; i < numFlows; i++) {
    BulkSendHelper bulk("ns3::TcpSocketFactory", InetSocketAddress(receiverInterfaces.GetAddress(i), port));
    bulk.SetAttribute("MaxBytes", UintegerValue(0));
    bulk.SetAttribute("SendSize", UintegerValue(mss));
    ApplicationContainer app = bulk.Install(senders.Get(i));

    struct configFlow flow = conf.flows[i];
    app.Start(flow.start);
    app.Stop(flow.stop);
    senderApps.Add(app);

    if (flow.stop > experimentDuration) {
      experimentDuration = flow.stop;
    }
  }
  
  PacketSinkHelper sink("ns3::TcpSocketFactory", InetSocketAddress(port));
  ApplicationContainer receiverApps = sink.Install(receivers);
  receiverApps.Start(Seconds(0.0));
  receiverApps.Stop(experimentDuration);

  NS_LOG_INFO("Setting up routing tables");

  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  NS_LOG_INFO("Scheduling link change events.");

  for (const auto& linkChange : conf.linkChanges) {
    Simulator::Schedule(linkChange.start, &HandleLinkChange, linkChange);
  }

  NS_LOG_INFO("Enabling pcap trace on switches before and after bottleneck.");

  std::string outputDirPath = "test";
  outputDirPath += "/" + GetDateTime("%m%d_%H%M%S") + "_" + configName;

  MakeDirectories(outputDirPath);
  
  toBottleneck.EnablePcap(outputDirPath + "/s1.pcap", r0Devices.Get(0), true, true);
  bottleneck.EnablePcap(outputDirPath + "/s3.pcap", r2Devices.Get(0), true, true);

  NS_LOG_INFO("Copying config file");

  CopyFile(configFilePath, outputDirPath + "/" + configName + ".yaml");

  NS_LOG_INFO("Enabling bottleneck queue tracing.");

  AsciiTraceHelper ascii;
  Ptr<OutputStreamWrapper> streamBytesInQueue = ascii.CreateFileStream(outputDirPath + "/s2-eth2.buffer");
  Config::ConnectWithoutContext("NodeList/1/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue<Packet>/BytesInQueue", MakeBoundCallback(&TraceQueue, streamBytesInQueue));

  NS_LOG_INFO("Scheduling tracing of cwnd/ssthresh etc.");

  currentCwnds = new uint32_t[numFlows]();
  currentSsthreshs = new uint32_t[numFlows]();

  // Tracing of cwnd/ssthresh... apparently needs to be configured after the application starts --> see examples/tcp/tcp-variants-comparison.cc
  for (int i = 0; i < numFlows; i++) {
    std::ostringstream addr;
    Ipv4Address ip = senderInterfaces.GetAddress(i);
    ip.Print(addr);
    Ptr<OutputStreamWrapper> streamCCValues = ascii.CreateFileStream(outputDirPath + "/" + addr.str() + ".bbr");
    Simulator::Schedule(conf.flows[i].start + Seconds(0.001), &StartTrace, streamCCValues, i, mss);
  }

  NS_LOG_INFO("Running simulation...");

  Simulator::Stop(experimentDuration);

  Simulator::Run();
  Simulator::Destroy();

  delete [] currentCwnds;
  delete [] currentSsthreshs;

  NS_LOG_INFO("Done.");

  // Can't use ns-3 logging as it is disabled in optimized builds
  std::cout << "Output written to: " << outputDirPath << std::endl;
  
  return 0;
}
