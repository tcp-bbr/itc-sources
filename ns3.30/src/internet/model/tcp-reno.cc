/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 ResiliNets, ITTC, University of Kansas
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "tcp-reno.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("TcpReno");
NS_OBJECT_ENSURE_REGISTERED (TcpReno);

TypeId
TcpReno::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::TcpReno")
    .SetParent<TcpNewReno> ()
    .AddConstructor<TcpReno> ()
    .SetGroupName ("Internet")
  ;
  return tid;
}

TcpReno::TcpReno (void)
  : TcpNewReno (),
    m_ackCnt (0)
{
  NS_LOG_FUNCTION (this);
}

TcpReno::TcpReno (const TcpReno& sock)
  : TcpNewReno (sock),
    m_ackCnt (sock.m_ackCnt)
{
  NS_LOG_FUNCTION (this);
}

TcpReno::~TcpReno (void)
{
  NS_LOG_FUNCTION (this);
}

Ptr<TcpCongestionOps>
TcpReno::Fork (void)
{
  return CopyObject<TcpReno> (this);
}

// Linux: net/ipv4/tcp_cong.c ll. 400-408
uint32_t
TcpReno::SlowStart (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked)
{
  NS_LOG_FUNCTION (this << tcb << segmentsAcked);

  uint32_t cwnd = std::min (tcb->GetCwndInSegments () + segmentsAcked, tcb->GetSsThreshInSegments ());

  segmentsAcked -= cwnd - tcb->GetCwndInSegments ();
  tcb->m_cWnd = cwnd * tcb->m_segmentSize; // No cwnd_clamp implemented in TcpSocketState

  NS_LOG_INFO ("In SlowStart, updated to cwnd " << tcb->m_cWnd << " ssthresh " << tcb->m_ssThresh);

  return segmentsAcked;
}

// Linux: net/ipv4/tcp_cong.c ll. 414-430
void
TcpReno::CongestionAvoidance (Ptr<TcpSocketState> tcb,
                                  uint32_t segmentsAcked)
{
  NS_LOG_FUNCTION (this << tcb << segmentsAcked);

  uint32_t segCwnd = tcb->GetCwndInSegments ();
  NS_ASSERT (segCwnd >= 1);

  uint32_t oldCwnd = segCwnd;
  uint32_t w = segCwnd;

  if (m_ackCnt >= w)
    {
      m_ackCnt = 0;
      segCwnd++;
    }

  m_ackCnt += segmentsAcked;

  if (m_ackCnt >= w)
    {
      uint32_t delta = m_ackCnt / w;

      m_ackCnt -= delta * w;
      segCwnd += delta;
    }
  tcb->m_cWnd = segCwnd * tcb->m_segmentSize; // No cwnd_clamp implemented in TcpSocketState

  if (segCwnd != oldCwnd)
    {
      NS_LOG_INFO ("In CongAvoid, updated to cwnd " << tcb->m_cWnd <<
                   " ssthresh " << tcb->m_ssThresh);
    }
}

std::string
TcpReno::GetName () const
{
  return "TcpReno";
}

// Linux: net/ipv4/tcp_cong.c ll. 440-455
void
TcpReno::IncreaseWindow (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked)
{
    NS_LOG_FUNCTION (this << tcb << segmentsAcked);

    // Check for cwnd limited missing, not implemented in TcpSocketState
    
    if (tcb->m_cWnd < tcb-> m_ssThresh)
      {
        segmentsAcked = SlowStart (tcb, segmentsAcked);
        if (segmentsAcked == 0)
          {
            return;
          }
      }

    CongestionAvoidance (tcb, segmentsAcked);
}

// Linux: net/ipv4/tcp_cong.c ll. 459-464
uint32_t
TcpReno::GetSsThresh (Ptr<const TcpSocketState> tcb,
                          uint32_t bytesInFlight)
{
  NS_LOG_FUNCTION (this << tcb << bytesInFlight);

  return std::max( tcb->m_cWnd.Get () >> 1, 2 * tcb->m_segmentSize);

}

} // namespace ns3
