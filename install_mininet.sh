#!/bin/bash
basedir=$PWD

apt-get update
apt-get install -y python3 python3-pip python-pip
pip install -r requirements.txt
pip3 install -r requirements.txt

git clone -b yaml https://gitlab.lrz.de/tcp-bbr/measurement-framework.git mininet
cd mininet
sudo ./install.sh

cd ..
