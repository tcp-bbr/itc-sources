import os
import argparse
import yaml
import numpy as np

def read_config(filename):
    with open(filename, 'r') as file:
        conf = yaml.safe_load(file)
        return conf

def write_config(filename, config):
    with open(filename, 'w') as file:
        yaml.dump(config, file)

def main():
    description = """
    Generate configs with different buffersizes from a base config.
    The buffersize is given in multiples of the BDP.
    Only the global RTT is considered for computing the BDP.
    """

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('baseconfig', metavar='BASE_CONFIG', help='Path to the base configuration file')
    parser.add_argument('--min', dest='min', type=float, default=0.1, help='Minimum buffersize (multiple of BDP)')
    parser.add_argument('--max', dest='max', type=float, default=3, help='Maximum buffersize (multiple of BDP)')
    parser.add_argument('--step', dest='step', type=float, default=0.1, help='Step size')

    args = parser.parse_args()

    config = read_config(args.baseconfig)
    name = os.path.splitext(os.path.basename(args.baseconfig))[0]

    rtt = config['rtt'].strip()
    if not rtt.endswith('ms'):
        print('RTT should be given in milliseconds. Terminating...')
        return
    rtt = float(rtt.replace('ms', '').strip())

    if not os.path.exists(name):
        os.makedirs(name)

    for multiplier in np.arange(args.min, args.max + args.step, args.step):
        config['latency'] = str(round(multiplier * rtt)) + 'ms'
        write_config(os.path.join(name, str(round(multiplier, 2)) + 'BDP.yaml'), config)

if __name__ == "__main__":
    main()