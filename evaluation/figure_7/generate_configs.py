import os
import argparse
import yaml

def read_config(filename):
    with open(filename, 'r') as file:
        conf = yaml.safe_load(file)
        return conf

def write_config(filename, config):
    with open(filename, 'w') as file:
        yaml.dump(config, file)

def main():
    description = """
    Generate configs for RTT unfairness by adding a flow with varying RTT to a base config.
    This flow is similar to the first flow in the base config but starts one second later and has a varying RTT.
    The configs are written to a new subdirectory named after the base config.
    """

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('baseconfig', metavar='BASE_CONFIG', help='Path to the base configuration file')
    parser.add_argument('--min', dest='min', type=int, default=10, help='Minimum RTT')
    parser.add_argument('--max', dest='max', type=int, default=100, help='Maximum RTT')
    parser.add_argument('--step', dest='step', type=int, default=10, help='Step size')

    args = parser.parse_args()

    config = read_config(args.baseconfig)
    name = os.path.splitext(os.path.basename(args.baseconfig))[0]

    if not os.path.exists(name):
        os.makedirs(name)

    if ('events' not in config):
        raise ValueError("No events specified")

    first_flow = next(flow for flow in config['events'] if flow['type'] == 'flow')
    config['events'].append({'type': 'flow', 'start': first_flow['start'] + 1, 'stop': first_flow['stop'], 'rtt': '0ms', 'algorithm': first_flow['algorithm']})

    for rtt in range(args.min, args.max + 1, args.step):
        config['events'][-1]['rtt'] = str(rtt) + 'ms'
        write_config(os.path.join(name, str(rtt) + 'ms.yaml'), config)

if __name__ == "__main__":
    main()