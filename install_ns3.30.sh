#!/bin/bash
basedir=$PWD
nsdir=ns-allinone-3.30.1/ns-3.30.1/

apt-get update
apt-get install -y python3 python3-pip python-pip
pip install -r requirements.txt
pip3 install -r requirements.txt

mkdir install3.30.1
cd install3.30.1
basedir=$PWD

# install ns-3.30.1
wget https://www.nsnam.org/releases/ns-allinone-3.30.1.tar.bz2
tar xjf ns-allinone-3.30.1.tar.bz2

# install yamlcpp
git clone https://github.com/jbeder/yaml-cpp.git yamlcpp
cd yamlcpp
mkdir build
cd build
cmake ..
make
make install
cd $basedir

# copy ns3 sources
cp -r ../ns3.30/* $nsdir

# build ns3
cd $nsdir
./waf configure --build-profile=optimized --enable-examples
./waf clean
./waf build
cd $basedir

cd ..